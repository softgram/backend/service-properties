FROM openjdk:20 as builder

WORKDIR /app

COPY ./build/libs/service-properties-1.0-SNAPSHOT.jar .

ENTRYPOINT java -jar ./service-properties-1.0-SNAPSHOT.jar