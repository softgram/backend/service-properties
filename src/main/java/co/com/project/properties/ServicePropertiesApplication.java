package co.com.project.properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ServicePropertiesApplication {

    public static void main(String... args) {
        SpringApplication.run(ServicePropertiesApplication.class);
    }

}
